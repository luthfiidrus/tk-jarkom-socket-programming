import socket

print("==========================================================")
print("Selamat Datang di Contoh Program Implementasi Dasar Socket")
print("==========================================================")
print("Berikut adalah daftar JOB yang dapat dieksekusi oleh program:")
print("    1. Melakukan operasi pertambahan matematika. Format perintah: tambah(angka1, angka2)")
print("    2. Menghitung banyaknya angka ganjil dan genap")
print("       pada suatu bilangan hasil perpangkatan. Format perintah: eo(angka,pangkat)")
print("    3. Menghitung angka fibonacci. Format perintah: fibo(angka yang diinginkan)")
print("==========================================================")
print("Berikut adalah CONTOH input/masukkan yang benar:")
print("    1. tambah:1,2")
print("    2. eo:2,34")
print("    3. fibo:10")
print("\nUntuk memberhentikan proses input, masukkan kata END")
print("==========================================================")

server_name = "127.0.0.1"
server_port = 2001
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect((server_name, server_port))
list_jobs = []

inpt = input("Masukkan PERINTAH job yang ingin dijalankan: ")
while(inpt.lower() != "end"):
    list_jobs.append(inpt)
    inpt = input("Masukkan PERINTAH job yang ingin dijalankan: ")
client_socket.send(' | '.join(list_jobs).encode())
reply = client_socket.recv(1024).decode()
print('Jawaban Worker:\n' + reply)
client_socket.close()
